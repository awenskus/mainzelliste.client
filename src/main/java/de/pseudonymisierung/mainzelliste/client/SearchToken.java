package de.pseudonymisierung.mainzelliste.client;

import de.pseudonymisierung.mainzelliste.client.ID;
import de.pseudonymisierung.mainzelliste.client.Token;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class SearchToken extends Token {

    private LinkedList<ID> searchIds = new LinkedList<ID>();;

	/**
	 * Create an instance without setting the token id.
	 */
	public SearchToken() {
		super();
	}
	
    /**
     * Add a patient to the list of patients for which data should be retreived.
     * 
     * @param id Permanent identifier of a patient.
     * @return The modified token object.
     */
    public SearchToken addSearchId(ID id) {
        this.searchIds.add(id);
        return this;
    }

	@Override
    public JSONObject toJSON() {
        try {
            JSONObject result = super.toJSON();
            result.put("type", "search");            
            JSONObject data = result.getJSONObject("data");
            JSONArray searchIds = new JSONArray();
            for (ID id : this.searchIds) {
                searchIds.put(id.toJSON());
            }
            data.put("searchIds", searchIds);
            return result;
        } catch (JSONException e) {
            throw new Error(e);
        }
    }

}